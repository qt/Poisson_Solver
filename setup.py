'''
    Execute with python3 setup.py build_ext --inplace
'''
import sys
import numpy
from setuptools import setup, find_packages
from Cython.Build import cythonize

install_requires = [
    'scipy',
    'numpy<2',
    'matplotlib',
]

setup(
    name='poisson',
    description='Poisson solver using finite volume',
    author='Poisson authors',
    license='BSD',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: Pre-alpha',
        'Programming Language :: Python :: 3.6',],
    packages=find_packages('.', exclude='2DEG_chris'),
    install_requires=install_requires,
    ext_modules=cythonize(['poisson/discrete/_finite_volume.pyx',
                           'poisson/discrete/_discrete_poisson.pyx',
                           'poisson/discrete/_linear_problem.pyx']),
    include_dirs=[numpy.get_include()]
)
